export class AppLaptopInformation {

    ellaptopInformation = document.getElementById('selected-laptop-information')
    ellaptopDescription = document.getElementById('laptopDescription')
    elStoreFeatureListDisplay = document.getElementById('featureList')
    ellaptopCard = document.getElementById('showlaptopDetails')
    elOnsalebadge = document.getElementById('saleBadge')
    laptop;

    setlaptop(laptop) {
        this.laptop = laptop
        this.render()
    }

    render() {

        if (!this.laptop) {
            return;
        }else{
            
        }

        // Clear the current laptop
        this.ellaptopInformation.innerHTML = ''
        

        // Create a new container for the laptop-image to be rendered in.
        const ellaptop = document.createElement( 'div' )

        const ellaptopPrice = document.getElementById('laptopPrice')
        ellaptopPrice.innerText = `${this.laptop.price}  NOK`

        const ellaptopName = document.getElementById( 'laptopName' )
        ellaptopName.innerText = this.laptop.name

        const ellaptopDescription = document.getElementById( 'laptopDescription' );
        ellaptopDescription.innerText = this.laptop.description

        const ellaptopFeatures = document.getElementById( 'laptopFeatures' );
        const featureList = this.laptop.features.map(feature => `<li>${feature}</li>`).join('');
        ellaptopFeatures.innerHTML = featureList;

        const ellaptopImageContainer = document.createElement('div')
        const image = new Image()
        image.width = 320
        image.onload = () => {
            ellaptopImageContainer.appendChild( image )
        }

        image.src = this.laptop.img;
        console.log( this.laptop.img )
        image.id="laptopImage"
        ellaptop.appendChild( ellaptopImageContainer )

        //checks if laptop is on sale and displays the "on sale"-badge
        if(this.laptop.onSale === true){
            this.elOnsalebadge.style.visibility = "visible"
        }else{
            this.elOnsalebadge.style.visibility = "hidden"
        }

        this.ellaptopInformation.appendChild( ellaptop )
    }
}
