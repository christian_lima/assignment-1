import { AppLaptop } from './laptop.js'

class App {
    constructor() {

        this.elStatus = document.getElementById('app-status')
        this.laptop = new AppLaptop()

                // Properties
                this.bankBalance = 1500;
                this.payBalance = 0;
                this.hasOutstandingLoan = false;
                this.numberOfTotalLoans = 0;
                this.outstandingLoanAmount= 0;
                this.payRate = 100;
                this.personName = "Joe Banker";
                this.ownedLaptops = [];
        
        // DOM
        this.workButton = document.getElementById('workButton')
        this.bankButton = document.getElementById('bankButton')
        this.getLoanButton = document.getElementById('loanButton')
        this.buyButton = document.getElementById('buyButton')
        this.repayLoanButton = document.getElementById('repayLoanButton')
        


    
    }

    async init() {
        await this.laptop.init()
        this.render()
        this.startEventListener()
        this.displayBalances()
        this.displayPersonDetails()
        this.checkOutstandingLoan()
        this.displayOwnedLaptops()
       
    }


    // Event Listeners for the various buttons 
    startEventListener() {
        this.workButton.addEventListener('click', this.work.bind(this))
        this.bankButton.addEventListener('click', this.updateBankBalance.bind(this))
        this.getLoanButton.addEventListener('click', this.getLoan.bind(this))
        this.buyButton.addEventListener('click', this.buyLaptop.bind(this))
        this.repayLoanButton.addEventListener('click', this.repayLoan.bind(this))
        
    }

    render() {
        this.laptop.render()
    }

    displayBalances(){
        document.getElementById("bankBalance").innerHTML = this.bankBalance + "  NOK"
        document.getElementById("payBalance").innerHTML = this.payBalance + "  NOK"
        this.checkOutstandingLoan()
        this.displayLoanBalance()
    }

    // Checks if there are any outstanding loans, returns true if that is the case, otherwise false.
    hasLoan(){
        if (this.outstandingLoanAmount > 0){
            return true;
        }else{
            return false;
        }
    }

    // payBalance increase, and display call function
    work(){
        this.payBalance += this.payRate;
        this.displayBalances()
    }



    // Pays outstandingLoan by using payBalance
    repayLoan(){
        if(this.payBalance < this.outstandingLoanAmount){
        this.outstandingLoanAmount = this.outstandingLoanAmount - this.payBalance;
        this.payBalance = 0;
        }else{
            let toBank = this.payBalance - this.outstandingLoanAmount;
            this.bankBalance += toBank;
            this.payBalance = 0;
            this.outstandingLoanAmount = 0;
        }
        
        this.displayBalances();
    }


    displayPersonDetails(){
        document.getElementById("personName").innerHTML = this.personName;
    }

    // the repay loan Button
    checkOutstandingLoan(){
        let elRepayLoanButton = document.getElementById("repayLoanButton");
        if(this.hasLoan()){
            elRepayLoanButton.style.display = "block";
        }else{
            elRepayLoanButton.style.display = "none";
        }
        console.log(this.hasLoan())
    }

    //transfers the payBalance to the bank balance
    updateBankBalance(){
        if (this.hasLoan()){
            let deductionAmount = (10 / 100) * this.payBalance;
            this.payBalance = this.payBalance - deductionAmount;
            this.outstandingLoanAmount = this.outstandingLoanAmount - deductionAmount;
            this.bankBalance += this.payBalance
            this.payBalance = 0
    
        }else{
            this.bankBalance += this.payBalance
            this.payBalance = 0
        }
        this.displayBalances()
    }


    // 'Get Loan' button, adds a loan to the users balance
    getLoan(){
        this.checkLoanConstraints(this.getLoanAmount())
        this.checkOutstandingLoan()
    }

    //gets the loan amount from the user with a prompt
    getLoanAmount(){
      let desiredAmount = prompt("Enter amount: ");
      if (desiredAmount == null || desiredAmount == "") {
        console.log("Exited loan application process..")
      } else {
        return desiredAmount;
      }
    }


    //checks the loan constraints
    checkLoanConstraints(desiredLoanAmount){
        if( desiredLoanAmount > 2*this.bankBalance ){
            alert("Amount too large. You'll only be able to loan: " + 2*this.bankBalance + " NOK")
        }
        if(this.hasLoan()){
        alert("Pay off your loan before applying for an additinal one.")
        }
        if(this.numberOfTotalLoans != 0 && this.ownedLaptops.length === 0){
            alert("You cannot buy a laptop whilst having more than 1 loan.")
        }
        else if( desiredLoanAmount < 2*this.bankBalance && !this.hasLoan() ){
            this.numberOfTotalLoans ++;
            this.updateLoanBalance(desiredLoanAmount)
            this.displayBalances()
        }
        }

        // Update loanBalance function
        updateLoanBalance(desiredLoanAmount){
            this.outstandingLoanAmount = desiredLoanAmount;
            this.displayBalances();
        }


        // Dependent upon whether or not the user has a outstandingLoan.
        // if that is the case the repayLoan button will appear.
        displayLoanBalance(){
            let title = document.getElementById("loanTitle");
            let balance = document.getElementById("loanBalance");
            if(this.hasLoan()){
                title.style.display ="block"
                balance.style.display = "block"
                title.innerHTML = "Loan:"
                balance.innerHTML = this.outstandingLoanAmount + " NOK";
            }else{
                title.style.display = "none"
                balance.style.display = "none"
            }
            this.displayLoanDetails();
        }

        // Displays current details regarding the loan.
        displayLoanDetails(){
            let loanDetails = document.getElementById("loanDetails")
            if(this.hasLoan()){
                loanDetails.style.display = "block"
                loanDetails.innerHTML = "Current debt: " + this.outstandingLoanAmount + " NOK. Please pay it back as soon as possible";
            }else{
                loanDetails.style.display = "none"
            }
        }

        // Lists previously purchased computers
        displayOwnedLaptops(){
            let elOwnedLaptops = document.getElementById("ownedLaptops")
            let elLaptopList = document.getElementById("laptopList")
            
            if (this.ownedLaptops.length === 0){
                elOwnedLaptops.innerHTML = "Personal inventory empty.";
            }else{
                elOwnedLaptops.innerHTML = "<b>Previous purchases: </b>"
                const laptops = this.ownedLaptops.map(name => `<li>${name}</li>`).join('');
                elLaptopList.innerHTML = laptops;
        }
    }
            // Checks if the user has selected a computer prior to pushing the buyButton, 
            // checks if the user is trying to purchase a computer with insuficcients funds, 
            // and puts a newly purchased computer in the inventory.  
        buyLaptop(){
            if(this.laptop.selectedLaptop === null){
                alert("Pick a laptop before pressing 'Purchase'.");
            }else{
                let laptopPrice = this.laptop.selectedLaptop.price;

            if (laptopPrice > this.bankBalance ){
                alert(`Insufficient funds. Try again when you have more money.`)
            }else{
                this.bankBalance = this.bankBalance - laptopPrice;
                this.displayBalances()
                this.ownedLaptops.push(this.laptop.selectedLaptop.name);
                alert("Transaction completed. You now own: " + this.laptop.selectedLaptop.name)
            }
           this.displayOwnedLaptops()

            }
            
        }

}

new App().init()
