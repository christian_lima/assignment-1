const BASE_URL = 'http://localhost:3000'

function extractJsonFromBody(response) {
    return response.json()
}

export function fetchLaptops() {
    return fetch(`${BASE_URL}/laptops`)
        .then(extractJsonFromBody)
}

export function fetchComputerById(id) {
    return fetch(`${BASE_URL}/laptops/${id}`)
        .then(extractJsonFromBody)
}

export function createComputer(computer) {
    // TODO: Implement POST for computer.
}
